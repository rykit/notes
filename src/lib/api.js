import axios from 'axios'

const api = axios.create({
  baseURL: 'https://60af82045b8c300017decf38.mockapi.io',
})

export default api
